# world

## Asia Continent

### CENTRAL ASIA

#### Kazakhstan

#### Kyrgyzstan

* [세계테마기행: 강인한 생명의 땅, 키르기스스탄](https://www.youtube.com/watch?v=iItT5uIZOKg)

#### Tajikistan

* [세계테마기행: 파미르를 걷다, 타지키스탄](https://www.youtube.com/watch?v=-J0dkV2RW4Q)

#### Turkmenistan

#### Uzbekistan

### EASTERN ASIA

#### China

#### Japan

#### Mongolia

#### North Korea

#### South Korea

#### Taiwan

### SOUTH-EASTERN ASIA

#### Brunei

#### Cambodia 

* [세계테마기행: 자연 그리고 인간의 땅, 캄보디아](https://www.youtube.com/watch?v=dUB-5XuSfLo)

#### Indonesia

#### Laos 

* [세계테마기행: 순수와 모험의 땅, 라오스](https://www.youtube.com/watch?v=4HLfX0G2nGw)

#### Malaysia

* [세계테마기행: 공존의 땅, 말레이시아](https://www.youtube.com/watch?v=9uyw0vJJcd8)

#### Myanmar

* [세계테마기행: 황금의 나라, 미얀마](https://www.youtube.com/watch?v=6AmvXai6hZo)
* [세계테마기행: 미얀마, 2,090km 이라와디 대장정](https://www.youtube.com/watch?v=iU46KyD0hHI)
* [세계테마기행: 순수의 나라 미얀마](https://www.youtube.com/watch?v=H9snok8UmJA)

#### Philippines

#### Singapore

#### Thailand

* [세계테마기행: 사왓디 캅! 태국](https://www.youtube.com/watch?v=xt_56hORR8M)
* [세계테마기행: 따스한 숨결, 태국](https://www.youtube.com/watch?v=J3QHlvbjGS4)
* [세계테마기행: 천국의 미소, 타이](https://www.youtube.com/watch?v=Bk_CFtf2vTE)

#### Timor-Leste

* [세계테마기행: 본디아 동티모르](https://www.youtube.com/watch?v=MufX8VxBR1M)

#### Viet Nam

### SOUTHERN ASIA

#### Afganistan

* [The history of Afganistan summarized](https://www.youtube.com/watch?v=T6usr-C3lcQ)
* [The Complete History of The Afghanistan War](https://www.youtube.com/watch?v=DHn3ledbz4Y)

#### Bangladesh 

* [세계테마기행: 강의 노래, 방글라데시](https://www.youtube.com/watch?v=s0zSml4uO2I)

#### Bhutan

* [세계테마기행: 히말라야 전설의 왕국, 부탄](https://www.youtube.com/watch?v=81uWjd9SYvk)

#### India

* [세계테마기행: 생에 한 번쯤은 인도](https://www.youtube.com/playlist?list=PLS2CWaUoKrr8HKOQw6vrlwIE6P63YTpIy)

#### Iran

* [세계테마기행: 우리가 몰랐던 이란](https://www.youtube.com/watch?v=ZL8iVy3-rNo)
* [세계테마기행: 두근두근 이란](https://www.youtube.com/playlist?list=PL8a1D1Nhc08bQfAfb5IGgiD8M62nGOcYt)

#### Maldives

#### Nepal

* [세계테마기행: 히말라야의 얼굴, 네팔](https://www.youtube.com/watch?v=RkY7lx8mwXI)
* [세계테마기행: 히말라야의 축복, 네팔](https://www.youtube.com/watch?v=7deJWiaT8GY)
* [세계테마기행: 신과 인간의 땅, 네팔](https://www.youtube.com/watch?v=3s10JKJGgM0)

#### Pakistan

* [세계테마기행: 아름다운 만남, 파키스탄](https://www.youtube.com/watch?v=wOIYoqE_Q7Q)
* [세계테마기행: 파키스탄, 카라코람 하이웨이를 가다](https://www.youtube.com/watch?v=n3GNxZLZYIU)

#### Sri Lanka

* [세계테마기행: 인도양의 진주, 스리랑카](https://www.youtube.com/watch?v=yNAOVjqr9G0)
* [세계테마기행: 공존의 힘, 스리랑카](https://www.youtube.com/watch?v=L-WQ8aw7Ugw)

### WESTERN ASIA

#### Armenia

#### Azerbaijan

#### Bahrain

#### Cyprus

#### Georgia

#### Iraq

#### Israel

#### Jordan

#### Kuwait

#### Lebanon

#### Oman

#### Qatar

#### Saudi Arabia

#### Palestine

#### Syria

#### Turkey

* [세계테마기행: 태양이 떠오르는 땅, 터키](https://www.youtube.com/watch?v=qTTN3bVZvKg)
* [세계테마기행: 천 개의 얼굴, 터키](https://www.youtube.com/watch?v=exIAsg-gdI4)

#### United Arab Emirates

#### Yemen

## Oceania Continent 
